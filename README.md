# Projeto Favorecidos - Itaú

## Objetivo do projeto

Este projeto tem como objetivo servir como parte da avaliação para a vaga de Engenheiro de Software Sênior do Itaú. A finalidade da aplicação é possibilitar que o usuário consulte sua lista de favorecidos através de uma api. Também foi criado um modelo de infraestrutura na cloud AWS que permite que a aplicação seja escalável, tenha alta disponibilidade e seja observável. 

## Descrição do projeto

O projeto consiste em dois microsserviços que compartilham o acesso à base de dados.
Requisitos para executar as aplicações localmente:
- Java 11
- Conexão com a internet (necessária para conexão com o banco de dados)

Ambas as aplicações podem ser executadas da mesma forma. Para executar a partir do código fonte, acessar a pasta raiz de cada projeto e executar `./gradlew bootRun`
Também é possível realizar download do arquivo .jar através do link disponibilizado juntamente com a descrição de cada projeto abaixo: `java -jar <aplicacao>.jar`

[Neste link](files/favorecidos.postman_collection.json) está disponível uma collection do postman para testar os serviços. 
[Neste link](files/favorecidos.postman_environment.json) está disponível o enviroment necessario para esta collection.

### [Cadastro de favorecidos](https://gitlab.com/favorecidos-itau/cadastro-favorecidos) 
Fornece api para cadastro e deleção de clientes, contas e favorecidos.
Confira o relatório de cobertura de teste [aqui](http://cadastro-favorecidos-coverage-report.s3-website.us-east-2.amazonaws.com/)

[Download .jar](files/cadastro-favorecidos-0.0.1-SNAPSHOT.jar)

### [Consulta de favorecidos](https://gitlab.com/favorecidos-itau/consulta-favorecidos)
Fornece api para autenticação do usuário e consulta dos respectivos favorecidos.
Confira o relatório de cobertura de teste [aqui](http://consulta-favorecidos-coverage-report.s3-website-sa-east-1.amazonaws.com/)


[Download .jar](files/consulta-favorecidos-0.0.1-SNAPSHOT.jar)

## Base de dados

Escolhi trabalhar com o SGBD MariaDb, já que é baseado no MySql e a instituição já usa o MySql. Alguns dos motivos pelos quais escolhi trabalhar com um banco relacional, são:

- Os dados precisam ser estruturados íntegros e consistentes. Um banco NoSql não garante estas propriedades;
- Garante controle transacional;
- Dados armazenados possuem tipos e podemos garantir que uma coluna não permita valor vazio.
  
Abaixo, segue a imagem da modelagem do banco e [aqui](files/create-database.sql) é possível acessar o script de criação.

![](files/images/modelo-bd.png)

## Arquitetura Cloud

Visando a facilidade e rapidez de escalonamento, escolhi utilizar o AWS ECS para hospedar a aplicação. Como o ECS trabalha com containeres e disponibliza o Fargate como ferramenta de gerenciamento, o trabalho e o tempo investidos para criar esta arquitetura e efetuar deploys seria reduzido. 

O planejamento seria criar um cluster com dois grupos de autoscaling, um para cada microsserviço. É previsível que o serviço de consulta de favorecidos terá uma carga maior do que o serviço de cadastro, sendo assim escalá-los separadamente é uma opção adequada.

Para manter o equilíbrio entre a carga de trabalho e para realizar o direcionamento das requisições, foi adicionado um load-balancer, que será responsável pelos dois grupos de auto scaling.

Para disponibilizar estas apis externamente, escolhi utilizar o cloudfront. Com ele é possível diminuir a latência sendo que este serviço armazena dados temporários em servidores Edge Location.

![](files/images/aws-architecture.png)


## Monitoramento e observabilidade

Primeiramente, escolhi o cloudwatch para monitorar os serviços, visto que sua utilização é relativamente simples e pode integrar em uma única visão todos os serviços utilizados dentro da AWS.

Para uma monitoração mais completa, seria interessante utilizar o Prometheus para colher as métricas e o Grafana para apresentar uma melhor visualização dos gráficos.Como ferramenta de logs escolheria o Splunk, que está entre os mais conceituados do mercado para centralização e gerenciamento de logs.

